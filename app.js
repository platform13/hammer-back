require('dotenv').config()
const { Console } = require('console')
const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000
const defaultLoad = process.env.DEFAULTLOAD || 20;
const { fib, dist } = require('cpu-benchmark')

app.use(cors())

app.get('/', (req, res) => { 
    const duration = fib(defaultLoad)
    res.json({ timeTaken: duration });
})

app.get('/fib/:load', (req, res) => {
    if (req.params.load < 36) {
        fibLoad = req.params.load
    } else {
        console.log('load ', req.params.load, ' is too big, max is 35, defaulting to ', defaultLoad, ' instead')
        fibLoad = defaultLoad
    }
    const duration = fib(fibLoad)
    res.json({ timeTaken: duration });
})

app.listen(port, () => {
    console.log(`Hammer listening at http://localhost:${port}`)
})